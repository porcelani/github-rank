## GithubRank

## Execute
-  Compile  `sbt compile`
-  Test  `sbt test`
-  Integration Test  `sbt it:test`
-  Coverage  `sbt coverage test coverageReport`
-  Scala Style  `sbt scalastyle`
-  Package  `sbt package`
-  Start App  `sbt run`


## API Documentation:
### contributors
```
curl -X GET -H "Content-Type: application/json" "localhost:8080/org/<ORGANIZATION_NAME>/contributors"
---
200 OK
{
   "contributors":[
      {
         "contributions":xxx,
         "name":"xxx"
      },
      {
         "contributions":xxx,
         "name":"xxx"
      }
      ...
   ]
}


```

## References:
- [Documents](doc)
- https://github.com/akka/akka-http-quickstart-scala.g8
   - https://developer.lightbend.com/guides/akka-http-quickstart-scala/backend-actor.html
   - https://github.com/spray/spray-json
   - https://doc.akka.io/docs/akka-http/current/client-side/request-and-response.html
- https://github.com/scoverage/sbt-scoverage
- http://www.scalastyle.org/
- https://github.com/mefellows/sbt-dotenv
- https://docs.github.com/en/rest/reference/rate-limit
```
curl \
-H "Accept: application/vnd.github.v3+json" \
-H "Authorization: bearer <TOKEN>" \
https://api.github.com/rate_limit

```

