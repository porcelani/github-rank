package com.porcelani.model

case class GithubUser(
                       login: String,
                       contributions: Int
                     )

