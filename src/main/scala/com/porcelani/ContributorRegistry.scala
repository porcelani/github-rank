package com.porcelani

//#contributor-registry-actor

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import com.porcelani.service.GithubService
import com.porcelani.utils.Akka.AkkaExecutor._
import com.porcelani.utils.Config

import scala.collection.immutable

//#contributor-case-classes
final case class Contributor(name: String, contributions: Int)

final case class Contributors(contributors: immutable.Seq[Contributor])

object ContributorRegistry {

  // actor protocol
  sealed trait Command

  final case class GetContributors(name: String, replyTo: ActorRef[Contributors]) extends Command

  final case class ActionPerformed(description: String)

  def apply()(implicit githubService: GithubService): Behavior[Command] = registry(Set(), githubService)

  private def registry(contributors: Set[Contributor], githubService: GithubService): Behavior[Command] =
    Behaviors.receiveMessage {
      case GetContributors(name, replyTo) =>
        for {
          listOfContributors <- githubService.getContributors(name)
        } yield replyTo ! Contributors(listOfContributors)
        Behaviors.same
    }
}
