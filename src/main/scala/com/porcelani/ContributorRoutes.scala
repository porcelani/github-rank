package com.porcelani

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import scala.concurrent.Future
import com.porcelani.ContributorRegistry._
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

class ContributorRoutes(contributorRegistry: ActorRef[ContributorRegistry.Command])(implicit val system: ActorSystem[_]) {

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import JsonFormats._

  // If ask takes more time than this to complete the request is failed
  private implicit val timeout = Timeout.create(system.settings.config.getDuration("my-app.routes.ask-timeout"))

  def getContributors(name: String): Future[Contributors] =
    contributorRegistry.ask(GetContributors(name, _))

  val contributorRoutes: Route =
  pathPrefix("org" / Segment / "contributors") { name =>
    concat(
      pathEnd {
        concat(
          get {
            complete(getContributors(name))
          })
      })
  }
}
