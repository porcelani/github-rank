package com.porcelani.utils

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

object Akka {
  object AkkaExecutor {
    implicit val system = ActorSystem()
    implicit val executor: ExecutionContext = system.dispatcher
  }
}
