package com.porcelani.utils

import com.typesafe.config.{ConfigFactory, Config => TypeConfig}

trait Config {
  private val config = ConfigFactory.load()
  val githubUrl = "https://api.github.com"
  val ghToken: String = config.getString("gh-token")
}
