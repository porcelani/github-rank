package com.porcelani

import com.porcelani.ContributorRegistry.ActionPerformed
import com.porcelani.model.{GithubUser, Repo}
import spray.json.DefaultJsonProtocol

object JsonFormats  {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._

  implicit val contributorJsonFormat = jsonFormat2(Contributor)
  implicit val contributorsJsonFormat = jsonFormat1(Contributors)

  implicit val repoJsonFormat = jsonFormat1(Repo)
  implicit val githubUserJsonFormat = jsonFormat2(GithubUser)

  implicit val actionPerformedJsonFormat = jsonFormat1(ActionPerformed)
}
