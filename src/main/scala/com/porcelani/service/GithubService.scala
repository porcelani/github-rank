package com.porcelani.service

import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.porcelani.model.{GithubUser, Repo}
import com.porcelani.utils.Akka.AkkaExecutor._
import com.porcelani.utils.Config
import com.porcelani.{Contributor, JsonFormats}
import spray.json.DefaultJsonProtocol._

import scala.concurrent.Future


trait GithubServiceComponent {
  val githubService = new GithubService
}

class GithubService extends Config {
  val log: LoggingAdapter = Logging(system, getClass)

  import JsonFormats._

  def getRepos(organization: String): Future[List[Repo]] = {
    getRepos(organization, 1)
  }

  private def getRepos(organization: String, page: Int): Future[List[Repo]] = {
    val getReposURL = s"${githubUrl}/orgs/$organization/repos?page=$page"
    log.debug(getReposURL)

    val responseFuture = Http().singleRequest(HttpRequest(GET,
      headers = getHeaders,
      uri = Uri(getReposURL)
    ))
    responseFuture.flatMap(f = resp => resp.status match {
      case OK => for {
        list1 <- Unmarshal(resp.entity).to[List[Repo]]
        list2 <- if (list1.nonEmpty) getRepos(organization, page + 1) else Future(List.empty[Repo])
      } yield list1 ++ list2
      case _ => Future(List.empty[Repo])
    })
  }

  def getContributors(organization: String, repo: String): Future[List[GithubUser]] = {
    getContributors(organization, repo, 1)
  }

  private def getContributors(organization: String, repo: String, page: Int): Future[List[GithubUser]] = {
    val getContributorsURL = s"${githubUrl}/repos/$organization/$repo/contributors?page=$page"
    log.debug(getContributorsURL)

    val responseFuture = Http().singleRequest(HttpRequest(GET,
      headers = getHeaders,
      uri = Uri(getContributorsURL)
    ))
    responseFuture.flatMap(f = resp => resp.status match {
      case OK => for {
        list1 <- Unmarshal(resp.entity).to[List[GithubUser]]
        list2 <- if (list1.nonEmpty) getContributors(organization, repo, page + 1) else Future(List.empty[GithubUser])
      } yield list1 ++ list2
      case _ => Future(List.empty)
    })
  }

  def getContributors(organization: String): Future[List[Contributor]] = {
    for {
      repos <- getRepos(organization)
      listOfGithubUser <- requestAllContributors(organization, repos)
    } yield groupToContributorLogin(listOfGithubUser)
  }

  def groupToContributorLogin(listOfGithubUser: List[GithubUser]): List[Contributor] = {
    listOfGithubUser.groupBy(_.login)
      .map { kv =>
        Contributor(kv._1, kv._2.map(_.contributions).sum)
      }.toList.sortBy(_.contributions)(Ordering.Int.reverse)
  }

  private final def getHeaders = {
    Seq(
      RawHeader("Accept", "application/vnd.github.v3+json"),
      RawHeader("Authorization", s"bearer $ghToken")
    )
  }

  private def requestAllContributors(organization: String, repos: List[Repo]) = {
    val listOfFutures: List[Future[List[GithubUser]]] = repos.map(repo => getContributors(organization, repo.name))
    val futureOfList: Future[List[GithubUser]] = Future.foldLeft(listOfFutures)(List.empty[GithubUser])(_ ++ _)
    futureOfList
  }
}
