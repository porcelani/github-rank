package com.porcelani.service

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.porcelani.Contributor
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

class GithubServiceItSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with GithubServiceComponent {

  implicit val timeout: Duration = 10 seconds

  "GithubService" should {
    "return the repos of some organization" in {
      val response = githubService.getRepos("pa-to")
      val result = Await.result(response, timeout)
      result.length shouldEqual 1
    }

    "return the contributions of some organization and some repo" in {
      val response = githubService.getContributors("pa-to", "pa.to")
      val result = Await.result(response, timeout)
      result.length shouldEqual 2
    }

    "return the contributions of some organization" in {
      val response:Future[List[Contributor]] = githubService.getContributors("pa-to")
      val result = Await.result(response, timeout)

      result.length shouldEqual 2
      result should contain(Contributor("gutobortolozzo",38))
      result should contain(Contributor("heronoyama",2))
    }
  }
}
