package com.porcelani.service

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.porcelani.Contributor
import com.porcelani.model.GithubUser
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._
import scala.language.postfixOps

class GithubServiceSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with GithubServiceComponent {

  implicit val timeout: Duration = 10 seconds

  "GithubService" should {
    "return the List(Contributor) group by name" in {
      val users = List(
        GithubUser("a", 1),
        GithubUser("b", 1), GithubUser("b", 1),
        GithubUser("c", 1), GithubUser("c", 1), GithubUser("c", 1)
      )

      val service = new GithubService()

      service.groupToContributorLogin(users) should be(List(Contributor("c", 3), Contributor("b", 2), Contributor("a", 1)))
    }
  }
}
