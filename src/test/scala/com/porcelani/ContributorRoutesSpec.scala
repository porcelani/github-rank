package com.porcelani

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.porcelani.service.GithubService
import com.porcelani.utils.Config
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.Future

class ContributorRoutesSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with Config{

  lazy val testKit = ActorTestKit()

  implicit def typedSystem = testKit.system

  override def createActorSystem(): akka.actor.ActorSystem =
    testKit.system.classicSystem

  implicit val service: GithubService = new GithubService {
    override def getContributors(organization: String): Future[List[Contributor]] = {
      Future(List(Contributor("porcelani", 1)))
    }
  }

  val contributorRegistry = testKit.spawn(ContributorRegistry())
  lazy val routes = new ContributorRoutes(contributorRegistry).contributorRoutes

  "ContributorRoutes" should {
    "return no contributors if no present (GET /contributors)" in {
      val request = HttpRequest(uri = "/org/test/contributors")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[String] should ===("""{"contributors":[{"contributions":1,"name":"porcelani"}]}""")
      }
    }
  }
}
