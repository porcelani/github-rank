
lazy val akkaHttpVersion = "10.2.3"
lazy val akkaVersion = "2.6.12"
lazy val IntegrationTest = config("it") extend(Test)

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings,
    inThisBuild(List(
      organization := "com.porcelani",
      scalaVersion := "2.13.4"
    )),
    name := "github-rank",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.0",

      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % "3.1.4" % Test
    )
  )

coverageMinimum := 90
coverageFailOnMinimum := true
coverageExcludedPackages := "<empty>;" +
  "com.porcelani.QuickstartApp;"


envVars in Test := (envFromFile in Test).value
